const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];

function kata1() {
    let p;
    for (let count = 1; count <= 25; count++) {
        p = count + " &nbsp;";
        let resultado1 = document.createElement("span");
        resultado1.innerHTML = p;
        document.getElementById("kata1").appendChild(resultado1);
    }
}
kata1();


function kata2() {
    let p;
    for (let count = 25; count > 0; count--) {
        p = count + " &nbsp;";
        let resultado2 = document.createElement("span");
        resultado2.innerHTML = p;
        document.getElementById("kata2").appendChild(resultado2);
    }
}
kata2();

function kata3() {
    let p;
    for (let count = -1; count >= -25; count--) {
        p = count + " &nbsp;";
        let resultado3 = document.createElement("span");
        resultado3.innerHTML = p;
        document.getElementById("kata3").appendChild(resultado3);
    }

}
kata3();


function kata4() {
    let p;
    for (let count = -25; count < 0; count++) {
        p = count + " &nbsp;";
        let resultado4 = document.createElement("span");
        resultado4.innerHTML = p;
        document.getElementById("kata4").appendChild(resultado4);
    }

}
kata4();


function kata5() {
    let p;
    for (let count = 25; count > -26; count = count - 2) {
        p = count + " &nbsp;";
        let resultado5 = document.createElement("span");
        resultado5.innerHTML = p;
        document.getElementById("kata5").appendChild(resultado5);
    }

}
kata5();


function kata6() {
    for (let count = 3; count <= 100; count = count + 3) {
        p = count + " &nbsp;";
        let resultado6 = document.createElement("span");
        resultado6.innerHTML = p;
        document.getElementById("kata6").appendChild(resultado6);
    }
}
kata6();


function kata7() {
    for (let count = 7; count <= 100; count = count + 7) {
        p = count + " &nbsp;";
        let resultado7 = document.createElement("span");
        resultado7.innerHTML = p;
        document.getElementById("kata7").appendChild(resultado7);
    }
}
kata7();


function kata8() {
    for (let count = 100; count > 1; count--) {
        if (count % 3 == 0 && count % 7 == 0) {
            p = count + " &nbsp;";
            let resultado8 = document.createElement("span");
            resultado8.innerHTML = p;
            document.getElementById("kata8").appendChild(resultado8);

        }
    }
}
kata8();


function kata9() {
    for (let count = 5; count <= 100; count = count + 5) {
        if (count % 2 != 0) {
            p = count + " &nbsp;";
            let resultado9 = document.createElement("span")
            resultado9.innerHTML = p;
            document.getElementById("kata9").appendChild(resultado9);
        }
    }
}
kata9();


function kata10() {
    for (let count = 0; count < sampleArray.length; count++) {
        let p = sampleArray[count]
        resultado10 = document.createElement("span");
        resultado10.innerHTML = p + " &nbsp;";
        document.getElementById("kata10").appendChild(resultado10);
    }

}
kata10();


function kata11() {
    for (let count = 0; count < sampleArray.length; count++) {
        if (sampleArray[count] % 2 == 0) {
            let p = sampleArray[count];
            resultado11 = document.createElement("span");
            resultado11.innerHTML = p + " &nbsp;";
            document.getElementById("kata11").appendChild(resultado11);
        }
    }
}
kata11();


function kata12() {
    for (let count = 0; count < sampleArray.length; count++) {
        if (sampleArray[count] % 2 != 0) {
            let p = sampleArray[count];
            resultado12 = document.createElement("span");
            resultado12.innerHTML = p + " &nbsp;";
            document.getElementById("kata12").appendChild(resultado12);
        }
    }
}
kata12();


function kata13() {
    for (let count = 0; count < sampleArray.length; count++) {
        if (sampleArray[count] % 8 == 0) {
            let p = sampleArray[count]
            resultado13 = document.createElement("span");
            resultado13.innerHTML = p + " &nbsp;";
            document.getElementById("kata13").appendChild(resultado13);
        }
    }
}
kata13();


function kata14() {
    for (let count = 0; count < sampleArray.length; count++) {
        let p = sampleArray[count] ** 2;
        resultado14 = document.createElement("span");
        resultado14.innerHTML = p + " &nbsp;";
        document.getElementById("kata14").appendChild(resultado14);
    }
}
kata14();


function kata15() {
    let soma = 0;
    for (let count = 1; count <= 20; count++) {
        soma = count + soma;
        resultado15 = document.createElement("span");
        resultado15.innerHTML = soma + " &nbsp;";
        document.getElementById("kata15").appendChild(resultado15);
    }
}
kata15();


function kata16() {
    let soma = 0;
    for (let count = 0; count < sampleArray.length; count++) {
        soma = sampleArray[count] + soma;
        resultado16 = document.createElement("span");
        resultado16.innerHTML = soma + " &nbsp;";
        document.getElementById("kata16").appendChild(resultado16);
    }

}
kata16();


function kata17() {
    let menor = sampleArray[0]
    for (let count = 0; count < sampleArray.length; count++) {
        if (sampleArray[count] < menor) {
            menor = sampleArray[count];
        }
    }
    resultado17 = document.createElement("span");
    resultado17.innerHTML = menor + " &nbsp;";
    document.getElementById("kata17").appendChild(resultado17);
}
kata17();


function kata18() {
    let maior = sampleArray[0]
    for (let count = 0; count < sampleArray.length; count++) {
        if (sampleArray[count] > maior) {
            maior = sampleArray[count];
        }
    }
    resultado18 = document.createElement("span");
    resultado18.innerHTML = maior + " &nbsp;";
    document.getElementById("kata18").appendChild(resultado18);
}
kata18();